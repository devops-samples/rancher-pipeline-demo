# 1.ingress工作负载信息

```shell
# 镜像
rancher/nginx-ingress-controller:nginx-0.43.0-rancher1

# 主机匹配
beta.kubernetes.io/os != windows
node-role.kubernetes.io/worker

# 默认端口
http 80
https 443

# 修改端口的方式，如果用hostport模式默认端口被占用会报错,可以用下面的方式修改，记得同时修改端口映射
入口/命令>命令   末尾增加  --http-port=8880 --https-port=8443

# 健康检查
GET /healthz
PORT 10254

# 标签
app=ingress-nginx

# 注释
prometheus.io/port=10254
prometheus.io/scrap=true
```

# 2.ingress 负载均衡测试
路径 | 工作负载
---|---
/demo/(.*) | rancher-pipeline-demo

> 注释

```shell
# 路径重写去掉前缀，然后路由转发给对应的服务
nginx.ingress.kubernetes.io/rewrite-target: /$1
```

# 3.ingress canary测试
路径 | 工作负载
---|---
/demo/(.*) | rancher-pipeline-demo
/demo/(.*) | rancher-pipeline-demo-V2

> 注释

```shell
# 路径重写去掉前缀，然后路由转发给对应的服务
nginx.ingress.kubernetes.io/rewrite-target: /$1
```

```shell
# 不支持灰度发布ingress默认配置
nginx.ingress.kubernetes.io/canary: false
```
```shell
# 灰度发布基于自定义的cookie，cookie的key是自己定的，cookie的value可以为：never/always/任意值
# 下面这个例子当且仅当cookie为version:always会路由到该负载
# 开启灰度发布
nginx.ingress.kubernetes.io/canary: true
# 定义cookie的key为version
nginx.ingress.kubernetes.io/canary-by-cookie: version
```
```shell
# 灰度发布基于自定义的header，header的key是自己定的，header的value可以为：never/always/任意值
# 下面这个例子当且仅当header为version:always会路由到该负载
# 开启灰度发布
nginx.ingress.kubernetes.io/canary: true
# 定义header的key为version
nginx.ingress.kubernetes.io/canary-by-header: version
```
```shell
# 灰度发布基于自定义的header，header的key是自己定的，header的value可以为：never/always/任意值
# 下面这个例子当且仅当header为version:V2会路由到该负载
# 开启灰度发布
nginx.ingress.kubernetes.io/canary: true
# 定义header的key为version
nginx.ingress.kubernetes.io/canary-by-header: version
# 定义header的value为V2
nginx.ingress.kubernetes.io/canary-by-header-value: V2
```
```shell
# 灰度发布基于权重
# 下面这个例子有33%的机会 会路由到该负载
# 开启灰度发布
nginx.ingress.kubernetes.io/canary: true
# 定义权重值权重从0-100
nginx.ingress.kubernetes.io/canary-weight: 33
```