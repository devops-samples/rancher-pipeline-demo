package com.tdk.rancher.pipeline.demo.testingress;

import java.util.HashMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试ingress
 *
 * @author: taodingkai
 * @modified:
 * @version: 2021/4/14 15:36
 */
@RestController
public class TestIngressController {

  @Value("${current_version}")
  private String currentVersion;

  @GetMapping("/version")
  public HashMap<String, Object> version() {
    HashMap<String, Object> result = new HashMap<>();
    result.put("currentVersion", currentVersion);
    return result;
  }
}
