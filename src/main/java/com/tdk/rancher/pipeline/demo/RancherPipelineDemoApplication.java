package com.tdk.rancher.pipeline.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RancherPipelineDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RancherPipelineDemoApplication.class, args);
	}

}
