# <center>jenkins参数化构建</center>

> git代码：https://gitee.com/Squirrel_Aha/rancher-pipeline-demo.git

## 1.流水线代码
```groovy
//jenkins+rancher流水线测试
pipeline {
	agent any
	stages {
		stage('git代码拉取') {
			//拉代码
			steps {
				checkout([$class:'GitSCM',branches:[[name:'*/master']],extensions:[],userRemoteConfigs:[[credentialsId:'gitee',url:'https://gitee.com/Squirrel_Aha/rancher-pipeline-demo.git']]])
			}
		}
		stage('maven打包') {
			//打包
			steps {
				sh "mvn clean -U install -Dmaven.test.skip=true"
			}
		}
		stage('打docker镜像') {
			// 打镜像
			steps {
				// sh "mvn dockerfile:build"
				sh "docker build -t 192.168.16.44:5000/rancher-pipeline-demo:latest --build-arg JAR_FILE_NAME=rancher-pipeline-demo-0.0.1-SNAPSHOT.jar --build-arg JAR_PORT=8888 ./target"
			}
		}
		stage('推送镜像到仓库') {
			// 推送镜像
			steps {
				// sh "mvn dockerfile:build"
				sh "docker push 192.168.16.44:5000/rancher-pipeline-demo:latest"
			}
		}
		stage('部署服务') {
			// 通知rancher部署服务
			steps {
				rancherRedeploy alwaysPull: true, credential: 'rancher', images: '192.168.16.44:5000/rancher-pipeline-demo:latest', workload: '/project/c-rw7dq:p-4fdxr/workloads/deployment:test:rancher-pipeline-demo'
			}
		}
	}
}
```
## 2.改造流水线
### 2.1 改造后的流水线
```groovy
//jenkins+rancher流水线测试
pipeline {
	agent any
	stages {
		stage('git代码拉取') {
			//拉代码
			steps {
				checkout([$class:'GitSCM',branches:[[name:'$git_branche']],extensions:[],userRemoteConfigs:[[credentialsId:'$git_credential',url:'$git_url']]])
			}
		}
		stage('maven打包') {
			//打包
			steps {
				sh "mvn clean -U install -Dmaven.test.skip=true"
			}
		}
		stage('打docker镜像') {
			// 打镜像
			steps {
				// sh "mvn dockerfile:build"
				sh "docker build -t $docker_registry_url/$project_name:$docker_tag --build-arg JAR_FILE_NAME=$project_name-0.0.1-SNAPSHOT.jar --build-arg JAR_PORT=$project_port ./target"
			}
		}
		stage('推送镜像到仓库') {
			// 推送镜像
			steps {
				// sh "mvn dockerfile:build"
				sh "docker push $docker_registry_url/$project_name:$docker_tag"
			}
		}
		stage('部署服务') {
			// 通知rancher部署服务
			steps {
				rancherRedeploy alwaysPull: true, credential: '$rancher_credential', images: '$docker_registry_url/$project_name:$docker_tag', workload: '$rancher_workload'
			}
		}
	}
}
```
### 2.2 改造后的流水线用到的变量

变量名|变量描述
|---|---|
project_name|项目名
git_branche|git分支
git_credential|git仓库凭据
git_url|git仓库地址
docker_registry_url|docker镜像仓库地址
docker_tag|docker镜像tag
project_port|docker容器要暴露的服务端口
rancher_credential|rancher凭据
rancher_workload|rancher工作负载路径  

## 3.可联动的参数化构建
### 3.1 Active Choices Plug-in和Git Parameter
> 可联动的参数化构建改造需要安装Active Choices Plug-in和Git Parameter
### 3.2 参数配置
#### 3.2.1 参数化构建过程-添加参数

> Git Parameter

Name|Description|Parameter Type|Default Value
|---|---|---|---|
git_branche|git分支|Branche|origin/master|
> Active Choices Parameter

Name|Script|Fallback Script|Description|Choice Type
|---|---|---|---|---|
task_name|return ["demo1","demo2"];|return ["nothing to choose"];|任务名|Single Select

> Active Choices Reactive Parameter

Name|Script|Fallback Script|Description|Choice Type|Referenced parameters
|---|---|---|---|---|---|
project_name|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["rancher-pipeline-demo"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|项目名|Single Select|task_name|
git_credential|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["gitee"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|git仓库凭据|Single Select|task_name|
git_url|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["https://gitee.com/Squirrel_Aha/rancher-pipeline-demo.git"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|git仓库地址|Single Select|task_name|
docker_registry_url|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["192.168.16.44:5000"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|docker镜像仓库地址|Single Select|task_name
docker_tag|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["latest"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|docker镜像tag|Single Select|task_name
project_port|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["8888"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|docker容器要暴露的服务端口|Single Select|task_name
rancher_credential|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["rancher"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|rancher凭据|Single Select|task_name
rancher_workload|switch(task_name){<br/>&nbsp;&nbsp;&nbsp;&nbsp;case "demo1":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["/project/c-s6thx:p-gbdf2/workloads/deployment:test:demo1"];<br/> &nbsp;&nbsp;&nbsp;&nbsp;case "demo2":<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return  ["/project/c-s6thx:p-gbdf2/workloads/deployment:test:demo2"];<br/>&nbsp;&nbsp;&nbsp;&nbsp;default:<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return ["nothing to choose"];<br/>}<br/>|return ["nothing to choose"];|rancher工作负载路径|Single Select|task_name
最终效果
![最终效果](https://images.gitee.com/uploads/images/2021/0427/181554_f1a938d3_1539250.png)